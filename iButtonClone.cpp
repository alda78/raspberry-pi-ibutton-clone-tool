//modification of https://blog.danman.eu/cloning-ibutton-using-rw1990-and-avr/

#include <bcm2835.h>
//#include "../Adafruit-RPi-LCD-master/gpio.h"
//#include "../Adafruit-RPi-LCD-master/lcd.h"
#include <stdio.h>

#define PIN     RPI_BPLUS_GPIO_J8_07 // 11 or 07

char data[8];

void delay_us(int us) {
    bcm2835_delayMicroseconds(us);
}

void up()
{
    bcm2835_gpio_fsel(PIN, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_set(PIN);
}

void down()
{
    bcm2835_gpio_fsel(PIN, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_clr(PIN);
}

void send(unsigned char byte)
{
    for (int bit=0; bit<8; bit++)
    {
        down();
        delay_us((byte & 1) ? 8 : 44);

        up();        
        delay_us((byte & 1) ? 44 : 8);

        byte = byte >> 1;
    }
    
    delay_us(1);
}

void write(unsigned char byte)
{
    for (int bit=0; bit<8; bit++)
    {
        down();
        delay_us((byte & 1) ? 50 : 1);
        
        up();
        delay_us(10000);
        
        byte = byte >> 1;
    }
}

int recv()
{
    unsigned char byte=0;

    for (int bit=0; bit<8; bit++)
    {
        down();
        delay_us(8);
        
        up();
        delay_us(8);
    
        byte >>= 1;
        if (bcm2835_gpio_lev(PIN))
            byte |= 0x80;

        delay_us(32);
    }

    return byte;
}

void sendR() {
    down();
    delay_us(1024);

    up();
    delay_us(1);
}

int waitP()
{
    //while (!bcm2835_gpio_lev(PIN))
    //  delay_us(1);
    
    int count = 10000;
    while (bcm2835_gpio_lev(PIN)) {
        if (--count <= 0)
            return 0;
        delay_us(1);
    }
    while (!bcm2835_gpio_lev(PIN))
        delay_us(1);

    delay_us(1);

    return 1;
}

void print(unsigned char data[8]) {
    printf("\033[01m"); //bold
    for (int i=0; i<8; ++i)
        printf("%02x", data[i]);
    printf("\033[0m"); //reset
}

bool verify(unsigned char data1[8], unsigned char data2[8]) {
    for (int i=0; i<8; ++i)
        if(data1[i] != data2[i]) return false;
    return true;
}

int read_code(unsigned char data[8])
{
    sendR();
    if (!waitP())
        return 0;

    send(0x33);

    for (int b=0; b < 8; b++)
        data[b] = recv();
        
    return 1;
}

int write_code(unsigned char data[8])
{
    sendR();
    if (!waitP())
        return 0;

    send(0xD5);

    write(data[0]);
    write(data[1]);
    write(data[2]);
    write(data[3]);
    write(data[4]);
    write(data[5]);
    write(data[6]);
    write(data[7]);

    delay_us(16000);

    sendR();
    waitP();
    
    return 1;
}

// unsigned char code[8] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
unsigned char burnCode[8] = {0x01, 0x6b, 0x4f, 0xff, 0x0e, 0x00, 0x00, 0x9f}; // my blue iButton code

char hex2i(char ch){
    if(ch == '0') return 0;
    if(ch == '1') return 1;
    if(ch == '2') return 2;
    if(ch == '3') return 3;
    if(ch == '4') return 4;
    if(ch == '5') return 5;
    if(ch == '6') return 6;
    if(ch == '7') return 7;
    if(ch == '8') return 8;
    if(ch == '9') return 9;
    if(ch == 'a') return 0x0a;
    if(ch == 'b') return 0x0b;
    if(ch == 'c') return 0x0c;
    if(ch == 'd') return 0x0d;
    if(ch == 'e') return 0x0e;
    if(ch == 'f') return 0x0f;
    return -1;
}

char hexhex2i(char ch0, char ch1){
    return hex2i(ch0)*16 + hex2i(ch1);
}

void setupBurnCode(char input[16]){
    for(int i=0; i<8; i++){
        burnCode[i] = hexhex2i(input[i*2], input[i*2 + 1]);
    }
}

int main(int argc, char* argv[])
{
    unsigned char data[8];
    
    //GPIO_open();
    //LCD_init(DISPLAY_SET);
    
    //LCD_printf("iButton read/write");
    
    printf("\n\033[01miButton Cloner (RW1990)\033[0m"); // bold ... reset
    printf("\n=======================");
    
    if (!bcm2835_init())
        return 1;

    bcm2835_gpio_fsel(PIN, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_set_pud(PIN, BCM2835_GPIO_PUD_OFF);
    bcm2835_gpio_set(PIN);
    
    
    if (argc == 1) {
        //read mode only
        printf("\niButton ReadOnly mode\n");
        while (1) {
            if (read_code(data)) {
                printf("\nCurrent iButton code: ");
                print(data);
                printf("\n");
                printf("\nFor writing this code into another iButton RW1990 device use: \nsudo ./iButtonClone ");
                print(data);
                printf("\n\n");
                break;
            }
            else{
                putchar('.');
                fflush(stdout);
                delay_us(500000);
            }
        }
    }

    if(argc == 2) {
        //wait for code, then write
        printf("\niButton Write mode");
        setupBurnCode(argv[1]);
        printf("\nWill try to write: ");
        print(burnCode);
        printf("\n");
        printf("\nWaiting for iButton ...");
        
        while (1) {
            if (read_code(data)) {
                printf("\nCurrent iButton code: ");
                print(data);
                printf("\n");
                break;
            }
            else {
                putchar('.');
                fflush(stdout);
                delay_us(500000);
            }
        }
        
        delay_us(16000);
        
        if (write_code(burnCode)) {
            printf("\nWriting new code into iButton RW1990 device: ");
            print(burnCode);
            printf("\n");
        }
        else
            printf("\n\033[31m!!!! Writing code failed !!!!\033[0m"); // red ... reset
            
        printf("\nVerifying code ... ");
        printf("\nCurrent iButton code: ");
        read_code(data);
        print(data);
        if(verify(data, burnCode)) printf("\nVerification \033[32mOK.\033[0m"); // green ... reset
        else printf("\nVerification \033[31mFAILED!!!\033[0m"); // red ... reset

        printf("\n\033[01mDONE.\033[0m\n"); // bold ... reset
    }
}
