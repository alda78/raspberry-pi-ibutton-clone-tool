# Raspberry Pi Dallas iButton DS1990R CLI Clone tool.
![Dallas iButton RW1990](./rw1990.png)  
It enables you to clone Dallas iButton DS1990R into another Dallas RW1990 by Raspberry PI1. 
CLI application is Written in C. It is inspired by https://gist.github.com/cameni

## How it works:
By running
```bash
sudo ./iButtonClone

iButton Cloner (RW1990)
=======================
iButton ReadOnly mode
.......
Current iButton code: cd34aa78ff34337a

For writing this code into another iButton RW1990 device use: sudo ./iButtonClone cd34aa78ff34337a
```
You can read iButton internal 8B number


And for example by running
```bash
sudo ./iButtonClone aa34bb78cc34dd78

iButton Cloner (RW1990)
=======================
iButton Write mode
Will try to write: aa34bb78cc34dd78
Waiting for iButton ...
..........
Current iButton code: cd34aa78ff34337a
Writing new code into iButton RW1990 device: aa34bb78cc34dd78
Verifying code ... 
Current iButton code: aa34bb78cc34dd78
DONE.
```
You can write iButton RW1990 internal 8B number. Written number will be verified automatically.

## How to connect Raspberry PI 1
Connect pull up rezistor 1k between 5V and GPIO7. Connect iButton with GPIO7 and GND.

![iButton connection to RPi1 GPIO pins](./rpi1-connection.png)

## How to compile application
For compile and build application simply run `build` script:
```bash
./build
```

Probably you will have problem with `gcc` and `bcm2835` lib dependency. You can solve it by this command sequence:
```bash
sudo apt update && sudo apt install gcc

wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.68.tar.gz
tar zxvf bcm2835-1.68.tar.gz
cd bcm2835-1.68
./configure
make
sudo make check
sudo make install
```
After that you can try to `./build` it again. If build is successful you will obtain `./iButtonClone` executable binnary.